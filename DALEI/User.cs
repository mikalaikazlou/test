﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DALEI
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SalaryId { get; set; }
        public Salary Salary { get; set; }
        public List<AddressUser> AddressUser { get; set; } = new List<AddressUser>();
        public List<Phone> Phones { get; set; } = new List<Phone>();
    }
}
