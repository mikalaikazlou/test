﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DALEI
{
    public class Salary
    {
        public int Id { get; set; }
        public int Sum { get; set; }
        public List<User> Users { get; set; } = new List<User>();
    }
}
