﻿using Microsoft.EntityFrameworkCore;

namespace DALEI
{
    public class AppContext : DbContext
    {
        public DbSet<Address> Addresses { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<Salary> Salaries { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<AddressUser> AddressUsers { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=./;Database=Tests;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AddressUser>().HasKey(t => new { t.UserId, t.AddressId });
            modelBuilder.Entity<AddressUser>()
                .HasOne(t => t.Address)
                .WithMany(t => t.AddressUser)
                .HasForeignKey(t => t.AddressId);

            modelBuilder.Entity<AddressUser>()
                .HasOne(t => t.User)
                .WithMany(t => t.AddressUser)
                .HasForeignKey(t => t.UserId);
        }
    }
}
