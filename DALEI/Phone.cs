﻿using System;

namespace DALEI
{
    public class Phone
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int AddressId { get; set; }
        public Address Address { get; set; }
    }
}
