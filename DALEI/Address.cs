﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DALEI
{
    public class Address
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CityId { get; set; }
        public City City { get; set; }
        public int UserId { get; set; }
        public List<AddressUser> AddressUser { get; set; } = new List<AddressUser>();
    }
}
